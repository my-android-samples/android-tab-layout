package com.hendisantika.androidtablayout

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

/**
 * Created by hendisantika on 02/11/18  07.30.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class SampleAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment? = when (position) {
        0 -> FragmentOne.newInstance()
        1 -> FragmentTwo.newInstance()
        2 -> FragmentThree.newInstance()
        else -> null
    }

    override fun getPageTitle(position: Int): CharSequence = when (position) {
        0 -> "TAB 1"
        1 -> "TAB 2"
        2 -> "TAB 3"
        else -> ""
    }

    override fun getCount(): Int = 3
}